package com.zhou.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {

    @Value("PROFILE")
    private String profile;

    @Value("LABEL")
    private String lable;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    @RequestMapping("get")
    public String getUser2(){
        System.out.println("访问了接口get");
        return profile + lable;
    }
}
