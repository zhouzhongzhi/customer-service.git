package com.zhou.lunch;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Import;
import com.zhou.init.SpringCloudInit;

@Import(SpringCloudInit.class)
public class Application {

    public static void main(String args[]){
        SpringApplication.run(Application.class, args);
    }
}
